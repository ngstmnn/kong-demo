= Welcome

This is a showcase to demonstrate a very simple usage of the https://konghq.com[Kong API gateway^].

In this tutorial you will:

* Start with a simple API that is not protected at all
* Add Kong as an API gateway to protect this API
* Add a user interface to access the Kong Admin API
* Add Prometheus and Grafana to monitor API usage

== Prerequisites

This tutorial makes use of https://www.docker.com/[Docker^] and https://docs.docker.com/compose/[Docker Compose^]. So you will need an up to date installation of these tools to follow the steps of this tutorial.

Additionally you will need a tool to send HTTP requests. The easiest way to do this is https://curl.haxx.se/[cURL^], so all examples in this tutorial will contain the required cURL command.

When using cURL you also may want to use https://stedolan.github.io/jq/[jq^] to have a nicer visualisation of the responses Kong returns. Simply append `| jq` to the cURL commands in this tutorial.

== Referrence files

The files that are created in each step of the tutorial can be downloaded from the xref:{page-component-version}@kong-demo::appendix.adoc[Appendix].

xref:{page-component-version}@kong-demo::01-api-only.adoc[Start the tutorial.]
