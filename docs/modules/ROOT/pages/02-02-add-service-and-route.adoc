= Add service and route

To make our _rover-api_ service accessible again, we will have to tell Kong two things:

* that the API exists and how kong can call it
* to add an endpoint that is accessible from outside the (docker)network

All this is done by adding a few lines to the configuration file we just created.

== Add a service

Kong and our API are both running in containers that belong to the same network. So from the perspective of Kong the API is available on a host named `rover-api` on port `8080`. So we add the following to our configuration:

[source,yaml]
----
services:
  - name: rover-api
    host: rover-api
    port: 80
    path: /rovers
----

== Add a route

Now that Kong knows where to find our API we can add a route Kong will listen on. The following lines will add a route that forwards all requests made to `http://<Kong-Server>/rovers` that have one of the verbs ("GET","POST","PUT","DELETE") to our API.

[NOTE]
====
We don't need to reference the service, as we can define the route nested within the service.

Make sure you use the right indention in the yml file.
====

[source,yaml]
----
    routes:
      - name: rover-api-route
        paths:
          - /rovers
        methods:
          - GET
          - POST
          - PUT
          - DELETE
----

== Call the API

Start the services again with:

[source,bash]
----
docker-compose up -d
----

Now the API can be called again with the same xref:{page-component-version}@kong-demo::01-api-only.adoc#_call_the_api[(cURL-)commands] we used when we only had the API deployed. But now all communication is routed through our API gateway. We can even prove this by adding the `-v` option to the cURL commands to display the headers returned. These headers are added by the Kong API gateway:

[source]
----
< X-Kong-Upstream-Latency: 1
< X-Kong-Proxy-Latency: 0
< Via: kong/1.4.2
----

Stop the services again to add some more lines to the configuration.

[source,bash]
----
docker-compose down
----

xref:{page-component-version}@kong-demo::02-03-protect-the-api.adoc[Continue to the next section.]
